<?php
/**
 * Template Name: Pagina Home
 *
 *
 * @package Odin
 * @since 2.2.0
 */

get_header();
?>

	<main id="content" class="col-12 page-home" tabindex="-1" role="main">
		<div class="row">
			<section id="banner" class="col-12">
				<div id="carousel-home" class="row carousel slide" data-ride="carousel">
					<!-- The slideshow -->
				  	<div class="carousel-inner">
				  		<div class="carousel-item active">
					    	<div class="container">
					    		<div class="row">
					    			<div class="col-6 col-md-6 offset-md-1">
						    			<h1 class="subtitle text-left">Novidade Zahil</h1>
							      		<h2 class="title text-left">Nova coletânea Salentein</h2>
							      		<p class="description">Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.</p>
							      		<a href="#" class="button">Experimente já!</a>
						    		</div>
						    		<div class="col-6 col-md-4 offset-md-1">
						    			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/vinho-slider.png">
						    		</div>
					    		</div>				    		
					    	</div>				      
					    </div>
					    <div class="carousel-item">
					    	<div class="container">
					    		<div class="row">
					    			<div class="col-6 col-md-6 offset-md-1">
						    			<h1 class="subtitle text-left">Novidade Zahil</h1>
							      		<h2 class="title text-left">Nova coletânea Salentein</h2>
							      		<p class="description">Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.</p>
							      		<a href="#" class="button">Experimente já!</a>
						    		</div>
						    		<div class="col-6 col-md-4 offset-md-1">
						    			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/vinho-slider.png">
						    		</div>
					    		</div>				    		
					    	</div>				      
					    </div>
					    <div class="carousel-item">
					    	<div class="container">
					    		<div class="row">
					    			<div class="col-6 col-md-6 offset-md-1">
						    			<h1 class="subtitle text-left">Novidade Zahil</h1>
							      		<h2 class="title text-left">Nova coletânea Salentein</h2>
							      		<p class="description">Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.</p>
							      		<a href="#" class="button">Experimente já!</a>
						    		</div>
						    		<div class="col-6 col-md-4 offset-md-1">
						    			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/vinho-slider.png">
						    		</div>
					    		</div>				    		
					    	</div>				      
					    </div>
				  </div>
				  <!-- Left and right controls -->
				  <a class="carousel-control-prev" href="#carousel-home" data-slide="prev">
				    <span class="carousel-control-prev-icon"></span>
				  </a>
				  <a class="carousel-control-next" href="#carousel-home" data-slide="next">
				    <span class="carousel-control-next-icon"></span>
				  </a>
				</div>
			</section>
			<section class="col-12 infos-pgto mt-3 mb-5">
				<div class="row">
					<div class="col-12 col-md-4 text-center">
						<p class="cartao">
							<strong class="d-block">Até 3 vezes sem juros</strong>
							<span>ou até 12 vezes com juros no cartão de crédito</span>
						</p>						
					</div>
					<div class="col-12 col-md-4 text-center">
						<p class="boleto">
							<strong class="d-block">5% de desconto</strong>
							<span>à vista no boleto bancário</span>
						</p>						
					</div>
					<div class="col-12 col-md-4 text-center">
						<p class="entrega">
							<strong class="d-block">Entregas</strong>
							<span>Na cidade do Rio de Janeiro</span>
						</p>						
					</div>
				</div>				
			</section>
			<section id="vinhos" class="container mt-5 mb-5 produtos">
				<h2 class="text-center special">Vinhos</h2>
				<div class="carousel-produtos">

					<?php
				    $args = array(
				      'post_type' => 'product',
				      'post_status' => 'publish',
				      'orderby' => 'menu_order',
				      'order' => 'ASC',
				      'posts_per_page'  => 9,
				      'tax_query'=> array(
				          array(
				              'taxonomy' => 'product_cat',
				              'field'    => 'name',
				              'terms'    => 'vinhos', 
				          )
				      ),
				    );
				    if ( query_posts( $args ) ) : ?>
				        <?php while ( have_posts() ) : the_post(); global $product; ?>
						<div class="content">
							<div class="row">
								<div class="col-12 col-md-5">
									<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
										<?php do_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 ); ?>
									</a>
								</div>
								<div class="col-12 col-md-7">
									<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
										<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
									</a>
									<div class="infos">
										<span class="pais <?php echo get_woo_product_attribute('pa_pais');?>"></span>
										<span class="tipo">
											<?php
											$tipo = get_woo_product_attribute('pa_tipo');
											echo $tipo;
											?>
										</span>
										<span class="uva">
											<?php
											$uva = get_woo_product_attribute('pa_uvas');
											echo $uva;
											?>
										</span>
									</div>
									<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>

									<?php do_action( 'woocommerce_after_shop_loop_item' );
									?>
								</div>
							</div>							
						</div>
				        <?php endwhile; ?>
				    <?php endif; ?>
				</div>
			</section>

			<?php /*<section id="emporio" class="col-12 mt-5 mb-5 produtos">
				<div class="container">
					<h2 class="text-center special">Empório</h2>
					<div class="carousel-produtos">

						<?php
					    $args = array(
					      'post_type' => 'product',
					      'post_status' => 'publish',
					      'orderby' => 'menu_order',
					      'order' => 'ASC',
					      'posts_per_page'  => 9,
					      'tax_query'=> array(
					          array(
					              'taxonomy' => 'product_cat',
					              'field'    => 'name',
					              'terms'    => 'emporio', 
					          )
					      ),
					    );
					    if ( query_posts( $args ) ) : ?>
					        <?php while ( have_posts() ) : the_post(); global $product; ?>
							<div class="content">
								<div class="row">
									<div class="col-12 text-center">
										<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
											<?php do_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 ); ?>
										</a>				
									</div>
									<div class="col-12 text-center">
										<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
											<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
										</a>
										<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
										<?php do_action( 'woocommerce_after_shop_loop_item' );
										?>
									</div>
								</div>
							</div>	
					        <?php endwhile; ?>
					    <?php endif; ?>
					</div>
				</div>				
			</section> */?>

			<section class="col-12 mt-5 mb-5">
				<div class="container newsletter">
					<div class="row">
						<div class="col-12 col-md-4 offset-md-2">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/wine-newsletter.jpg" class="img-fluid">
						</div>
						<div class="col-12 col-md-4 offset-md-1 content">
							<h3 class="special text-center">Novidades <span class="red">&</span><br/> Promoções</h3>
							<p class="text-center">Cadastre-se e receba novidades e promoções.</p>
							<div class="formulario">
								<?php echo do_shortcode('[contact-form-7 id="42" title="Newsletter"]'); ?>
							</div>
						</div>
						<div class="d-block col-md-1"></div>
					</div>
				</div>				
			</section>
		</div>
	</main><!-- #main -->

<?php
get_footer();
