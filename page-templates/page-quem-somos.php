<?php
/**
 * Template Name: Pagina Quem Somos
 *
 *
 * @package Odin
 * @since 2.2.0
 */

get_header();
?>

	<main id="content" class="col-12 page-quem-somos" tabindex="-1" role="main">
		<div class="row">
			<section class="container">
				<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();
				?>
					<h1 class="title text-center"><?php the_title(); ?></h1>
					<h2 class="sub-title text-center"><?php the_field('sub-titulo'); ?></h2>
						
					<div class="row compromisso">
						<div class="col-12 col-md-8 offset-md-2">
							<?php the_field('coluna_1'); ?>
						</div>
					</div>
					<div class="row quem-faz">
						<div class="col-12 col-md-2 offset-md-2 mb-4 text-center">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
						</div>
						<div class="col-12 col-md-6 conteudo">
							<h3 class="special"><?php the_field('sub-titulo_2'); ?></h3>
							<?php the_content(); ?>
						</div>						
					</div>
				<?php endwhile; ?>				
			</section>
		</div>
	</main><!-- #main -->

<?php
get_footer();
