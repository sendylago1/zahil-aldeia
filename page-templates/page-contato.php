<?php
/**
 * Template Name: Pagina Contato
 *
 *
 * @package Zahil
 * @since 1.0.0
 */

get_header();
?>

	<main id="content" class="col-12 interna" tabindex="-1" role="main">
		<div class="container">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				endwhile;
			?>
		</div>
	</main><!-- #main -->

<?php
get_footer();
