<?php
add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_style(
			'fas', get_template_directory_uri() .
				'/inc/webfonts/css/fontawesome.min.css',
			[], null
		);

} );

add_filter( 'wp_resource_hints', function ( $hints, $relation_type ) {

	// WP não tem suporte a preload (ideal neste caso) e seus atributos
	if ( 'prefetch' === $relation_type ) {
		$hints[] = [
				'href' => get_template_directory_uri() .
					'/inc/webfonts/fonts/fontawesome/4.7/normal.woff2'
			];
	}
	return $hints;
}, 10, 2 );
