<?php

if ( ! function_exists( 'theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function theme_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyfifteen
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	//load_theme_textdomain( 'theme_setup', get_template_directory() . '/languages' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	/**
	 * Register nav menus.
	 * This theme uses wp_nav_menu()
	 */
	register_nav_menus(
		array(
			'main-menu' => __( 'Menu Principal', 'odin' )
		)
	);
	register_nav_menus(
		array(
			'servico-menu' => __( 'Menu Serviço', 'odin' )
		)
	);
	register_nav_menus(
		array(
			'ajuda-menu' => __( 'Menu Ajuda', 'odin' )
		)
	);
	register_nav_menus(
		array(
			'social' => __( 'Social Links', 'odin' )
		)
	);

	/*
	 * Add post_thumbnails suport.
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Support Custom Header.
	 */
	/*$default = array(
		'width'         => 0,
		'height'        => 0,
		'flex-height'   => false,
		'flex-width'    => false,
		'header-text'   => false,
		'default-image' => '',
		'uploads'       => true,
	);
	add_theme_support( 'custom-header', $default );*/

	/**
	 * Support Custom Background.
	 */
	/*$defaults = array(
		'default-color' => '',
		'default-image' => '',
	);
	add_theme_support( 'custom-background', $defaults );*/

	/**
	 * Support Custom Editor Style.
	 */
	//add_editor_style( 'css/editor-style.css' );

	/**
	 * Add support for infinite scroll.
	 */
	add_theme_support(
		'infinite-scroll',
		array(
			'type'           => 'scroll',
			'footer_widgets' => false,
			'container'      => 'content',
			'wrapper'        => false,
			'render'         => false,
			'posts_per_page' => get_option( 'posts_per_page' )
		)
	);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	/*add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) ); */

	/*
	 * Enable support for custom logo.
	 *
	 * @since Twenty Fifteen 1.5
	 */
	/*add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
	) );*/

}
endif; // theme_setup
add_action( 'after_setup_theme', 'theme_setup' );

/**
 * Integrações.
 */
require_once get_template_directory() . '/inc/webfonts/webfonts.php';

function theme_enqueue_styles() {

    wp_register_style('bs4', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    
    $dependencies = array('bs4');

    wp_enqueue_style( 'bootstrapstarter-style', get_stylesheet_uri(), $dependencies ); 

    // Configs e customizações
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/estilos.css', ['fas'], null );
}
function theme_enqueue_scripts() {

	// Substitui jQuery padrão
	wp_deregister_script( 'jquery' );
	wp_register_script(
			'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js',
			['json2'], null, true
		);

	//jQuery Migrate
	wp_deregister_script( 'jquery-migrate' );
	wp_enqueue_script(
			'jquery-migrate', get_template_directory_uri() . '/assets/js/jquery-migrate.min.js',
			['jquery'], null, true
		);

	// Substitui jQuery UI padrão
	wp_deregister_script( 'jquery-ui-core' );
	wp_register_script(
			'jquery-ui-core', get_template_directory_uri() . '/assets/js/jquery-ui.min.js',
			['jquery'], null, true
		);
	wp_enqueue_script( 'jquery-ui-core' );
	
	wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/scripts.js', ['jquery'], null, true);

    $dependencies = array('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/assets/js/bootstrap.min.js', $dependencies, '4.5.0', true );

    // IE8 support for HTML5 elements and media queries
	wp_enqueue_script(
			'html5shiv',
			'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.0/html5shiv.min.js',
			[], null
		);
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

    // Estilos do WC para Gutenberg
	wp_deregister_style( 'wc-block-style' );

	// owlCarousel
	if( is_front_page() || is_404() ) {
		wp_enqueue_style(
				'owl', get_template_directory_uri() . '/assets/css/owl.carousel.min.css',
				[], null
			);
		wp_enqueue_style(
				'owl-theme', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css',
				['owl'], null
			);
		wp_enqueue_script(
				'owl', get_template_directory_uri() . '/assets/js/owl.carousel.min.js',
				['jquery'], null, true
			);
	}
    
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

function bootstrapstarter_wp_setup() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'bootstrapstarter_wp_setup' );

/**
 * Query WooCommerce activation
 *
 * @since  2.2.6
 *
 * @return boolean
 */
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		return class_exists( 'woocommerce' ) ? true : false;
	}
}
/**
 * WooCommerce compatibility files.
 */
if ( is_woocommerce_activated() ) {
	add_theme_support( 'woocommerce' );
}

/* Habilita WooCommerce Attributes no menu */
function wc_reg_tipo_for_menus( $register, $name = '' ) {
     if ( $name == 'pa_tipo' ) $register = true;
     return $register;
}
function wc_reg_pais_for_menus( $register, $name = '' ) {
     if ( $name == 'pa_pais' ) $register = true;
     return $register;
}
function wc_reg_uvas_for_menus( $register, $name = '' ) {
     if ( $name == 'pa_uvas' ) $register = true;
     return $register;
}
function wc_reg_estilo_for_menus( $register, $name = '' ) {
     if ( $name == 'pa_estilo' ) $register = true;
     return $register;
}
function wc_reg_selecoes_for_menus( $register, $name = '' ) {
     if ( $name == 'pa_selecoes' ) $register = true;
     return $register;
}
add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_tipo_for_menus', 1, 2);
add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_pais_for_menus', 1, 3);
add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_uvas_for_menus', 1, 4);
add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_estilo_for_menus', 1, 5);
add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_selecoes_for_menus', 1, 6);

/* Remodelando páginas woocommerce */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
/* Desabilita Cross Sell */
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20);


/* Configuracao de Listagem Pagina da Loja - BEGIN */
function only_vinhos_product( $q ){
	if( is_shop() ) {
		$product_type = array(
	      'taxonomy' => 'product_cat',
	      'field' => 'slug',
	      'terms' => 'vinhos'
	    );
	    $q->set('tax_query', array($product_type) );

	    $q->set( 'post_parent', 0 );
	}    
}
add_action( 'woocommerce_product_query', 'only_vinhos_product' );

/* Change number or products per row to 3 */
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 2; // 2 products per row
	}
}
add_filter('loop_shop_columns', 'loop_columns', 999);

/* Change number of related products output */ 
function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 3;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
  function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

/* Get products attributes */
function get_woo_product_attribute($attribute) {
    global $product;

    $abv = $product->get_attribute($attribute);
    if( $attribute == 'pa_pais') {  	
    	$abv = strtolower($abv);
		$from = array(
		  'á','à','â','â',
		  'ß','ç',
		  'è','é','ê',
		  'ì','í','î','Ñ',
		  'ò','ó','ô','ô',
		  'Ù','Ú','Û','Ü');
		$to = array(
		  'a','a','a','a',
		  'b','c',
		  'e','e','e',
		  'i','i','i','n',
		  'o','o','o','o',
		  'u','u','u','u');
		$abv = str_replace($from, $to, $abv);
    }
    
    if (empty($abv))
        return;
    echo __($abv, 'woocommerce');
}
add_action('woocommerce_shop_loop_item_title', 'get_woo_product_attribute', 15);

/* Widget Areas */
function widget_registration($name, $id, $description, $class, $beforeWidget, $afterWidget, $beforeTitle, $afterTitle){
    register_sidebar( array(
        'name' 			=> $name,
        'id' 			=> $id,
        'description' 	=> $description,
        'class'         => '',
        'before_widget' => $beforeWidget,
        'after_widget' 	=> $afterWidget,
        'before_title' 	=> $beforeTitle,
        'after_title' 	=> $afterTitle,
    ));
}
function multiple_widget_init(){
    widget_registration('Vinhos', 'vinhos-sidebar', 'Sidebar da loja/vinhos', '', '<aside id="%1$s" class="widget %2$s">', '</aside>', '<h3 class="widget-title">', '</h3>');
    widget_registration('Empório', 'emporio-sidebar', 'Sidebar do empório', '', '<aside id="%1$s" class="widget %2$s">', '</aside>', '<h3 class="widget-title">', '</h3>');
    widget_registration('Main', 'main-sidebar', '', '', '<aside id="%1$s" class="widget %2$s">', '</aside>', '<h3 class="widget-title">', '</h3>');
}
add_action('widgets_init', 'multiple_widget_init');

/* Limite resultados da busca */
function searchfilter($query) {
    if ($query->is_search && !is_admin() ) {
        $query->set( 'post_type', array('product') );
    }
return $query;
}
add_filter('pre_get_posts','searchfilter');

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 6;
  return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/* Configuração da Galeria WooCommerce */
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'cg_woocommerce_image_size_gallery_thumbnail', 99 );
function cg_woocommerce_image_size_gallery_thumbnail( $size ) {
    return array(
        'width'  => 100,
        'height' => 130,
        'crop'   => 1,
    );
}

add_filter( 'woocommerce_package_rates', 'bbloomer_unset_shipping_when_free_is_available_all_zones', 10, 2 );
function bbloomer_unset_shipping_when_free_is_available_all_zones( $rates, $package ) {
	$all_free_rates = array();
	     
	foreach ( $rates as $rate_id => $rate ) {
	      if ( 'free_shipping' === $rate->method_id ) {
	         $all_free_rates[ $rate_id ] = $rate;
	         break;
	      }
	}
	     
	if ( empty( $all_free_rates )) {
	        return $rates;
	} else {
	        return $all_free_rates;
	} 
}
?>