jQuery( document ).ready(function($) {

  var larguraBody = $('body').width();
  var filteredHrefs = [];

  //Menu Responsivo
  $('.abrir-menu').click(function(){
    $('.menu-principal').show("fade", 500);
    $('.menu-principal ul').addClass('transform');
    $('.conteudo-site').addClass('menu-aberto');
    return false;
  });
  $('.fechar-menu').click(function(){
    $('.menu-principal').hide("fade", {direction: "left"}, 500);
    $('.menu-principal ul').removeClass('transform');
    return false;
  });

  //Fixar Menu no Header
  var lastScrollTop = 0;

  $(window).scroll(function(event) {
    var st = $(this).scrollTop();
    if (st > 100) {
      $("header").addClass('fixar');
    } else {
      $("header").removeClass('fixar');
    }
    lastScrollTop = st;
  });

  // Scroll Suave
  $(".toscroll a[href^='#']").on('click', function(e) {
    e.preventDefault();

    var target = this.hash, 
    $target = $(target);   

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top - 84 }, 900, 'swing', function() {
       window.location.hash = target;
     });

    target = target.replace('#','');
    $('body').attr('id', 'step-'+target);

  });

  // Exibir busca
  $("header .menu-cart .cart-login .search").on('click', function(e){
    e.preventDefault();
    
    var compara = $("header .busca");

    if ( compara.is(".active") ) {
      $("header .busca").removeClass('active');
    } else {
      $("header .busca").addClass('active');
    }
  });

  //Carousel
  if (typeof jQuery.fn.owlCarousel === 'function') {
    $('.carousel-produtos').owlCarousel({
      loop:false,
      margin:15,
      dots: false,
      nav:true,
      navText : ["<div class='arrow-left'></div>","<div class='arrow-right'></div>"],
      center: false,
      autoplay:false,
      autoplayTimeout:2000,
      autoplayHoverPause:true,
      animateOut: 'fadeOut',
      responsive:{
        0:{
          items:1
        },
        991:{
          items:3
        }
      }
    });
  }

  $('.woof_list li').has('input:disabled').hide();

  $('.abre-filtros').click(function(){
    $(this).toggleClass('aberto');
    $('.widget-area .widget').slideToggle();
  });

 });