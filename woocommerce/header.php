<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/svg/Logo_Zahil.svg">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php if( is_search() ) { body_class('woocommerce'); } else { body_class(); } ?>>

    <header class="header">
        <div class="conteudo-menu container">
            <div class="row position-relative">
                <div class="col-12 col-lg-4 menu">
                    <nav class="menu-principal" id="menu-home">
                        <a href="" class="fechar-menu" title="">&#10005;</a>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'main-menu',
                                'container'      =>  '',
                                'items_wrap' => '<ul>%3$s</ul>'
                            )
                        );
                        ?>
                    </nav>
                </div>
                <div class="col-5 col-lg-4 text-center logo">                 
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <?php if(is_front_page()) : ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/Logo_Zahil_White.svg" class="img-fluid">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/Logo_Zahil.svg" class="img-fluid d-none fixo">
                        <?php else : ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/Logo_Zahil.svg" class="img-fluid fixo">
                        <?php endif; ?>
                    </a>                    
                </div>
                <div class="col-7 col-lg-4 menu-cart">
                    <a href="" class="abrir-menu"><i class="fa fa-bars"></i></a>
                    <div class="cart-login">
                         <!-- Search -->
                        <a href="#" class="search">
                            <?php if(is_front_page()) : ?>
                                <span class="img-fluid">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><defs><style>.cls-1{fill:#fff;}</style></defs><title>busca_white</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="busca_white" class="cls-1" d="M29.58,27.56l-7.92-7.91a12,12,0,0,0,2.63-7.51A12.15,12.15,0,1,0,12.14,24.29a12,12,0,0,0,7.51-2.63l7.91,7.92a1.43,1.43,0,0,0,1,.42,1.43,1.43,0,0,0,1-2.44ZM12.14,21.43a9.29,9.29,0,1,1,7.5-3.81,9.67,9.67,0,0,1-2,2A9.26,9.26,0,0,1,12.14,21.43Z"/></g></g></svg>
                                </span>
                                <span class="d-none fixo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><defs><style>.cls-2{fill:#363636;}</style></defs><title>busca_black</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="Busca_Black" class="cls-2" d="M29.58,27.56l-7.92-7.91a12,12,0,0,0,2.63-7.51A12.15,12.15,0,1,0,12.14,24.29a12,12,0,0,0,7.51-2.63l7.91,7.92a1.43,1.43,0,0,0,1,.42,1.43,1.43,0,0,0,1-2.44ZM12.14,21.43a9.29,9.29,0,1,1,7.5-3.81,9.67,9.67,0,0,1-2,2A9.26,9.26,0,0,1,12.14,21.43Z"/></g></g></svg>
                                </span>
                            <?php else : ?>
                                <span class="img-fluid fixo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><defs><style>.cls-2{fill:#363636;}</style></defs><title>busca_black</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="Busca_Black" class="cls-2" d="M29.58,27.56l-7.92-7.91a12,12,0,0,0,2.63-7.51A12.15,12.15,0,1,0,12.14,24.29a12,12,0,0,0,7.51-2.63l7.91,7.92a1.43,1.43,0,0,0,1,.42,1.43,1.43,0,0,0,1-2.44ZM12.14,21.43a9.29,9.29,0,1,1,7.5-3.81,9.67,9.67,0,0,1-2,2A9.26,9.26,0,0,1,12.14,21.43Z"/></g></g></svg>
                                </span>
                            <?php endif; ?>
                        </a>
                        <?php global $woocommerce; ?>
                        <!-- Minha Conta -->
                        <a href="<?php echo get_permalink( get_page_by_path( 'minha-conta' ) ); ?>" class="conta">
                            <?php if(is_front_page()) : ?>
                                <span class="img-fluid">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.43 30"><defs><style>.cls-1{fill:#fff;}</style></defs><title>user_white</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="user_white" class="cls-1" d="M21.09,15.23a8.57,8.57,0,1,0-10.76,0A15.75,15.75,0,0,0,0,30H2.86a12.86,12.86,0,1,1,25.71,0h2.86A15.75,15.75,0,0,0,21.09,15.23ZM10,8.57a5.72,5.72,0,1,1,5.71,5.72A5.71,5.71,0,0,1,10,8.57Z"/></g></g></svg>
                                </span>
                                <span class="d-none fixo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.43 30"><defs><style>.cls-2{fill:#363636;}</style></defs><title>usuario</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="usuario" class="cls-2" d="M21.09,15.23a8.57,8.57,0,1,0-10.76,0A15.75,15.75,0,0,0,0,30H2.86a12.86,12.86,0,1,1,25.71,0h2.86A15.75,15.75,0,0,0,21.09,15.23ZM10,8.57a5.72,5.72,0,1,1,5.71,5.72A5.71,5.71,0,0,1,10,8.57Z"/></g></g></svg>
                                </span>
                            <?php else : ?>
                                <span class="img-fluid fixo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.43 30"><defs><style>.cls-2{fill:#363636;}</style></defs><title>usuario</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="usuario" class="cls-2" d="M21.09,15.23a8.57,8.57,0,1,0-10.76,0A15.75,15.75,0,0,0,0,30H2.86a12.86,12.86,0,1,1,25.71,0h2.86A15.75,15.75,0,0,0,21.09,15.23ZM10,8.57a5.72,5.72,0,1,1,5.71,5.72A5.71,5.71,0,0,1,10,8.57Z"/></g></g></svg>
                                </span>
                            <?php endif; ?>
                        </a>
                        <!-- Carrinho -->
                        <a href="<?php echo wc_get_cart_url(); ?>" class="cart">
                            <?php if(is_front_page()) : ?>
                                <span class="img-fluid">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.71 30"><defs><style>.cls-1{fill:#fff;}</style></defs><title>carrinho_white</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="carrinho_white" class="cls-1" d="M45.43,7.72a1.43,1.43,0,0,0-1.14-.58H18.57a1.43,1.43,0,1,0,0,2.86h23.8L38.94,21.43H16.74L9.93,1A1.43,1.43,0,0,0,8.57,0H1.43a1.43,1.43,0,0,0,0,2.86H7.54l6.82,20.45a1.43,1.43,0,0,0,1.35,1h2.16a3.52,3.52,0,0,0-.73,2.14,3.58,3.58,0,0,0,7.15,0,3.52,3.52,0,0,0-.73-2.14h8.6a3.52,3.52,0,0,0-.73,2.14,3.57,3.57,0,1,0,7.14,0,3.52,3.52,0,0,0-.73-2.14H40a1.44,1.44,0,0,0,1.37-1L45.65,9A1.42,1.42,0,0,0,45.43,7.72Z"/></g></g></svg>
                                </span>
                                <span class="d-none fixo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.71 30"><defs><style>.cls-2{fill:#363636;}</style></defs><title>cartinho</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="cartinho" class="cls-2" d="M45.43,7.72a1.43,1.43,0,0,0-1.14-.58H18.57a1.43,1.43,0,1,0,0,2.86h23.8L38.94,21.43H16.74L9.93,1A1.43,1.43,0,0,0,8.57,0H1.43a1.43,1.43,0,0,0,0,2.86H7.54l6.82,20.45a1.43,1.43,0,0,0,1.35,1h2.16a3.52,3.52,0,0,0-.73,2.14,3.58,3.58,0,0,0,7.15,0,3.52,3.52,0,0,0-.73-2.14h8.6a3.52,3.52,0,0,0-.73,2.14,3.57,3.57,0,1,0,7.14,0,3.52,3.52,0,0,0-.73-2.14H40a1.44,1.44,0,0,0,1.37-1L45.65,9A1.42,1.42,0,0,0,45.43,7.72Z"/></g></g></svg>
                                </span>
                            <?php else : ?>
                                <span class="img-fluid fixo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.71 30"><defs><style>.cls-2{fill:#363636;}</style></defs><title>cartinho</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="cartinho" class="cls-2" d="M45.43,7.72a1.43,1.43,0,0,0-1.14-.58H18.57a1.43,1.43,0,1,0,0,2.86h23.8L38.94,21.43H16.74L9.93,1A1.43,1.43,0,0,0,8.57,0H1.43a1.43,1.43,0,0,0,0,2.86H7.54l6.82,20.45a1.43,1.43,0,0,0,1.35,1h2.16a3.52,3.52,0,0,0-.73,2.14,3.58,3.58,0,0,0,7.15,0,3.52,3.52,0,0,0-.73-2.14h8.6a3.52,3.52,0,0,0-.73,2.14,3.57,3.57,0,1,0,7.14,0,3.52,3.52,0,0,0-.73-2.14H40a1.44,1.44,0,0,0,1.37-1L45.65,9A1.42,1.42,0,0,0,45.43,7.72Z"/></g></g></svg>
                                </span>
                            <?php endif; ?>                            
                            <span class="quantidade">
                                <?php if ( $woocommerce->cart->cart_contents_count != 0 ) : ?>
                                    <?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?>
                                <?php else : ?>
                                    <?php echo "0"; ?>
                                <?php endif; ?>
                            </span>
                        </a>
                    </div>
                </div>
                <!-- /menu-cart -->
            </div>
        </div>
        <div class="container d-none busca">
            <form method="get" id="searchform" class="form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
                <div class="input-group">
                    <input type="search" class="form-control" name="s" id="s" value="<?php echo get_search_query(); ?>" placeholder="Buscar..." />
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default" value="<?php esc_attr_e( 'Search', 'odin' ); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><defs><style>.cls-1{fill:#fff;}</style></defs><title>busca_white</title><g id="Camada_2" data-name="Camada 2"><g id="Camada_1-2" data-name="Camada 1"><path id="busca_white" class="cls-1" d="M29.58,27.56l-7.92-7.91a12,12,0,0,0,2.63-7.51A12.15,12.15,0,1,0,12.14,24.29a12,12,0,0,0,7.51-2.63l7.91,7.92a1.43,1.43,0,0,0,1,.42,1.43,1.43,0,0,0,1-2.44ZM12.14,21.43a9.29,9.29,0,1,1,7.5-3.81,9.67,9.67,0,0,1-2,2A9.26,9.26,0,0,1,12.14,21.43Z"/></g></g></svg>
                        </button>
                    </span><!-- /input-group-btn -->
                </div><!-- /input-group -->
            </form><!-- /searchform -->
        </div>
    </header>

    <div class="container-fluid">
        <div class="row">