<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>	
	<div class="row">
		<?php
		$terms = get_the_terms( $product->ID, 'product_cat' );
        $nterms = get_the_terms( $product->ID, 'product_tag'  );
        foreach ($terms  as $term  ) {
            $product_cat_id = $term->term_id;
            $product_cat_name = $term->name;
            break;
        }
		?>
		<?php if ( (single_cat_title('', false) == 'Empório') || ($product_cat_name == 'Empório' ) ) : ?>
			<div class="col-12 text-center">
				<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
					<?php do_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 ); ?>
				</a>				
			</div>
			<div class="col-12 text-center">
				<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
					<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
				</a>
				<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
				<?php do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
				
		<?php else : ?>
			<div class="col-12 col-md-6">
				<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
					<?php do_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 ); ?>
				</a>
			</div>
			<div class="col-12 col-md-6">
				<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
					<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
				</a>
				<div class="infos">
					<span class="pais <?php echo get_woo_product_attribute('pa_pais');?>"></span>
					<span class="tipo">
						<?php
						$tipo = get_woo_product_attribute('pa_tipo');
						echo $tipo;
						?>
					</span>
					<span class="uva">
						<?php
						$uva = get_woo_product_attribute('pa_uvas');
						echo $uva;
						?>
					</span>
				</div>
				<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>

				<?php do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
		<?php endif; ?>		
	</div>	
</li>
