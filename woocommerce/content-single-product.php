<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'interna-produto', $product ); ?>>	
	<div class="col-12">
		<div class="row">
			<div class="col-12 text-left">
				<div class="container">
					<p class="breadcrumbs">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Voltar a Home">Home</a>
						<span class="sep"></span>
						<span><?php the_title(); ?></span>
					</p>
				</div>
			</div>
			<?php
			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
			?>
			
			<?php // Get Produtor
			$produtor = get_field('produtor');
			?>

			<div class="summary entry-summary">

				<h1 class="product_title entry-title"><?php the_title(); ?></h1>
				<div class="mini-info">
					<p>
						<?php if( get_field('safra') ) : ?>
							<?php echo get_field('safra'); ?>
						<?php endif; ?>
						<?php if( get_field('safra') && get_field('nome_do_produtor') ) : ?>
							<?php echo " | "; ?>
						<?php endif; ?>
						<?php if( $produtor ) : ?>
							<?php echo $produtor->post_title; ?>
						<?php endif; ?>
					</p>
					<p>
						<span class="pais"><?php echo $product->get_attribute('pa_pais'); ?></span>
						<?php if( get_field('regiao') ) : ?>
							<span class="regiao"><?php the_field('regiao'); ?></span>
						<?php endif; ?>
					</p>
				</div>				
				<?php
				/**
				 * Hook: woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				?>
				<?php if( get_the_content() ) : ?>
					<div class="woocommerce-product-details__short-description"><?php the_content(); ?></div>
				<?php endif; ?>
				<?php
				do_action( 'woocommerce_single_product_summary');

				echo do_shortcode('[addtoany]'); 
				?>

			</div>
		</div>		
	</div>

	<?php $short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
	if ( $short_description ) : ?>
		<div class="col-12 text-center mais">
			<?php
			$terms = get_the_terms( $post->ID, 'product_cat' );
			foreach ( $terms as $term ) {
			    $product_cat_id = $term->term_id;
			    if( $term = get_term_by( 'id', $product_cat_id, 'product_cat' ) ){
			    	$categoria = $term->name;
			    }
			    break;
			}
			?>
			<?php if ( $categoria == 'Empório' ) : ?>
					<h2 class="special">Mais Informações</h2>
					<div class="content">
						<?php echo $short_description; // WPCS: XSS ok. ?>
					</div>
			<?php else : ?>
				<h2 class="special">Ficha Técnica</h2>
				<div class="content">
					<?php echo $short_description; // WPCS: XSS ok. ?>
				</div>
				<div class="ficha-tecnica">
					<div class="row">
						<div class="col-6 col-md-3 offset-md-3">
							<div class="item tipo">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/tipo.svg">
								<strong>Tipo</strong>
								<p><?php echo get_woo_product_attribute('pa_tipo');?></p>
							</div>
							<?php if( get_field('uvas') ) : ?>
							<div class="item uvas">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/uvas.svg">
								<strong>Uvas</strong>
								<p><?php the_field('uvas'); ?></p>
							</div>
							<?php endif; ?>
							<?php if( get_field('corpo') ) : ?>
							<div class="item corpo">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/estilo.svg">
								<strong>Corpo</strong>
								<p><?php the_field('corpo'); ?></p>
							</div>
							<?php endif; ?>
						</div>
						<div class="col-6 col-md-3">
							<?php if( get_field('teor_alcoolico') ) : ?>
							<div class="item teor-alcoolico">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/teor.svg">
								<strong>Teor Alcoólico</strong>
								<p><?php the_field('teor_alcoolico'); ?></p>
							</div>
							<?php endif; ?>
							<?php if( get_field('organico_e_cia') ) : ?>
							<div class="item organico-e-cia">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/organico.svg">
								<strong>Orgânico & Cia</strong>
								<p><?php the_field('organico_e_cia'); ?></p>
							</div>
							<?php endif; ?>
							<?php if( get_field('madeira') ) : ?>
							<div class="item madeira">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/svg/safra.svg">
								<strong>Madeira</strong>
								<p><?php the_field('madeira'); ?></p>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<?php
	if( $produtor ): ?>
	<div class="col-12 produtor">
		<h2 class="special">Sobre o Produtor</h2>
		<div class="row">
		        <div class="col-3">
		        	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $produtor->ID ), 'large' ); ?>
				    <img src="<?php echo $image[0]; ?>" alt="<?php echo esc_html( $produtor->post_title ); ?>" />
				</div>
				<div class="col-9 text-left">
					<?php echo esc_html( $produtor->post_content ); ?>
				</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="col-12">
		<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );		
		?>
	</div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
