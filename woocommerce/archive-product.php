<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

?>
<div class="col-12 woocommerce-products-header mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="woocommerce-products-header__title page-title">
						<?php 
						if ( isset($_GET['swoof']) ) {
							$category = get_term_by( 'slug', get_query_var( 'term' ), 'product_cat' );
							echo $category->name;
							$description_category = $category->description;
						} else {
							if( is_tax() ) {
								$atributo = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );								
								if( single_cat_title('', false) == "Empório" ) {
									$category = get_term_by( 'slug', 'emporio', 'product_cat' );
									echo $category->name; // will show the name
									$description_atributo = $category->description;
								} else {
									echo $atributo->name; // will show the name
									$description_atributo = $atributo->description;
								}								
							} elseif( is_shop() ) {
								echo "Vinhos";
							} else {
								woocommerce_page_title();
							}
						} ?>
					</h1>
				<?php endif; ?>
				<?php if( isset($_GET['swoof']) ) { ?>
					<p><?php echo $description_category; ?></p>
				<?php } elseif( is_tax() ) { ?>
					<div class="term-description">
						<p><?php echo $description_atributo; ?></p>
					</div>
				<?php } elseif( is_shop() ) {

					$args  = array(
					    'taxonomy' => 'product_cat',
					    'field' => 'slug',
					    'terms' => 'vinhos'
					);
					$terms = wp_get_post_terms($post->ID, 'product_cat', $args);
					$count = count($terms);
					if ($count > 0) {
					    foreach ($terms as $term) {
					        //$description = $term->description;
					    }
					    ?>
					    <div class="term-description">
							<p><?php echo $description; ?></p>
						</div>
					<?php } ?>					
					
				<?php } else {
					/**
					 * Hook: woocommerce_archive_description.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );
				} ?>
				<?php echo $term_object->description; ?>
			</div>
			<div class="offset-md-1 col-md-3">
				<?php
				if ( is_product_category() ){
				    global $wp_query;
				    $cat = $wp_query->get_queried_object();
				    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
				    $image = wp_get_attachment_url( $thumbnail_id );
				    if ( $image ) {
					    echo '<img src="' . $image . '" alt="' . $cat->name . '" />';
					}
				}
				?>
			</div>
		</div>
	</div>	
</div>
<div class="container">
	<div class="row">
		<?php if ( !is_search() ) : ?>
		<aside id="sidebar" class="col-md-3 widget-area">
			<h3 class="abre-filtros">FILTRO </h3>
			<?php if ( single_cat_title('', false) == 'Empório' ) :
				dynamic_sidebar('emporio-sidebar');
			else :
				dynamic_sidebar('vinhos-sidebar');
			endif; ?>
			
		</aside>
		<?php endif; ?>
		<div class="col-12 col-md-9 <?php if ( is_search() ){ echo "offset-md-1"; } ?>">
		<?php
		if ( woocommerce_product_loop() ) {

			woocommerce_product_loop_start();

				while ( have_posts() ) {
					the_post();

					/**
					 * Hook: woocommerce_shop_loop.
					 */
					do_action( 'woocommerce_shop_loop' );

					wc_get_template_part( 'content', 'product' );
				}

			woocommerce_product_loop_end();

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		}  else {
			/**
			 * Hook: woocommerce_no_products_found.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action( 'woocommerce_no_products_found' );
		} ?>
		</div>
	</div>
</div>

<?php get_footer();
