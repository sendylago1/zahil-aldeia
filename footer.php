		</div><!-- /.row -->

		</div><!-- /.container -->

		<footer class="footer">
			<div class="infos">
				<div class="container">
				    <div class="row">
				    	<div class="col-12 col-md-2">
				    		<h5>Sobre o Serviço</h5>
				    		<nav class="menu-footer">
		                        <?php
		                        wp_nav_menu(
		                            array(
		                                'theme_location' => 'servico-menu',
		                                'container'      =>  '',
		                                'items_wrap' => '<ul>%3$s</ul>'
		                            )
		                        );
		                        ?>
		                    </nav>
				    	</div>
				    	<div class="col-12 col-md-2">
				    		<h5>Ajuda</h5>
				    		<nav class="menu-footer">
		                        <?php
		                        wp_nav_menu(
		                            array(
		                                'theme_location' => 'ajuda-menu',
		                                'container'      =>  '',
		                                'items_wrap' => '<ul>%3$s</ul>'
		                            )
		                        );
		                        ?>
		                    </nav>
				    	</div>
				    	<div class="col-12 col-md-2">
				    		<h5>Contato</h5>
				    		<strong>Zahil Vinhos Rio</strong>
				    		<?php
						 	$your_query = new WP_Query( 'pagename=fale-conosco' );
					    	while ( $your_query->have_posts() ) : $your_query->the_post();
							?>
					    		<p class="fone"><?php the_field('telefone'); ?></p>
					    		<p class="whatsapp"><?php the_field('whatsapp'); ?></p>
					    		<p class="instagram"><?php the_field('instagram'); ?></p>
					    		<p class="email"><?php the_field('e-mail'); ?></p>
				    		<?php
				    		endwhile;
						    wp_reset_postdata();
							?>
				    	</div>
				    	<div class="col-12 col-md-3 offset-md-3">
							<h5>Formas de Pagamento</h5>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/Cartoes_Pagamento_Footer.png" alt="bandeiras cartões">
						</div>
				    </div>
				</div>
			</div>
			<div class="copyright text-center">
				<div class="container">
				    <div class="row">
				    	<div class="col-12">
				    		<p>Copyright 2020 ZAHIL RJ - Todos os direitos reservados</p>
				    	</div>
				    	<div class="col-12">
				    		<p>Aprecie com Moderação. Produtos proibidos para menores de 18 anos. Se for beber, não dirija. Condições sujeitas a alteração sem aviso prévio. Para saber sobre as safras disponíveis fale com nossa equipe. Todas as garrafas contêm 750 ml., exceto quando indicado outro volume. Imagens meramente ilustrativas. Compras através do telefone, informações ou dúvidas entre em contato com nosso atendimento: 21 3940.0800</p>
				    	</div>			    	
				    </div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>